from os import path, system
import setuptools
from setuptools.command.install import install
import re, sys

module_path = path.join(path.dirname(__file__), 'FwDictionary/__init__.py')
version_line = [line for line in open(module_path)
                if line.startswith('__version__')][0]

__version__ = re.findall("\d+\.\d+\.\d+", version_line)[0]


class CustomInstall(install):
    """
    @description: Installs external FindWatt dependencies before installing
    the current package.
    """
    def run(self):
        self.install_fw_deps()
        install.run(self)
        self.download_wordnet()

    @staticmethod
    def install_fw_deps():
        if sys.version_info.major == 2:
            system("pip install git+https://bitbucket.org/FindWatt/fwtext")
            system("pip install git+https://bitbucket.org/FindWatt/fwfile")
        elif sys.version_info.major == 3:
            system("pip install git+https://bitbucket.org/FindWatt/fwtext")
            system("pip3 install git+https://bitbucket.org/FindWatt/fwfile")

    @staticmethod
    def download_wordnet():
        print("Downloading Wordnet corpus...")
        import nltk
        nltk.download("wordnet")


setuptools.setup(
    name="FwDictionary",
    version=__version__,
    url="https://bitbucket.org/FwPyClassification/fwdictionary",

    author="FindWatt",

    description="Classes to wrap vocabulary sets, lookup methods.",
    long_description=open('README.md').read(),

    packages=setuptools.find_packages(),
    include_package_data=True,
    package_data={'': ["data/*.txt"]},
    py_modules=['FwDictionary'],
    zip_safe=False,
    platforms='any',

    install_requires=[
        "pytest",
        "pytest-cov",
        "pylint",
        "nltk",
    ],
    cmdclass={'install': CustomInstall}
)
