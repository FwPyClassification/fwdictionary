Contains classes to wrap a set of vocabulary, and its words.
Those classes implement word-related and vocabulary-related methods.

Install with:

```
#!bash

pip install git+https://bitbucket.org/fwpy/fwdictionary
```


Usage:
```
#!python

import FwDictionary

# FwDictionary.FwDictionary wraps a set of vocabulary.
# Initialize it using the default vocab file
vocab = FwDictionary.FwDictionary()
# Or, optionally, pass the path to a vocabulary file
vocab = FwDictionary.FwDictionary(
    vocabulary_file="some/other/vocabulary_file.txt"
    )

# Check if a given word is in the vocab
"dog" in vocab
>> True

"Macarena" in vocab
>> False # Probably

# Accessing the words in the vocab, returns a Word object,
# that implements word-related methods.
type(vocab["dog"])
>> FwDictionary.Word

vocab["dog"].text
>>"dog"

vocab["dog"].synonyms
>>{"Canis_familiaris", "domestic_dog", "wiener", ...}

# Additionally, there's a look_up_synonyms function in the module.
# It's used by FwDictionary.Word, and it can be called independently.
FwDictionary.look_up_synonyms("left wing") # Looks up "left wing", "left-wing" and "left_wing"
>>{"left", "left-of-center", "leftist", ...}

```

This module requires nltk.corpus.wordnet

The setup.py includes code to download wordnet. But if it fails, try this:

```
#!python

import nltk
nltk.download("wordnet")
```