﻿'''
Test cases for FwDictionary/fwdictionary
'''
from __future__ import absolute_import
from os import path
from FwFile import folder_from_path, path_exists, file_exists
from FwDictionary import fwdictionary as fwdict


class TestDataPath():
    folder = folder_from_path(fwdict._vocab_full_path('fw_dictionary.txt'))

    def test_folder_exists(self):
        assert path_exists(self.folder)

    def test_files_present(self):
        a_files = [
            'combined-unigrams.txt',
            'fw_abbreviation_words.txt',
            'fw_accessory_words.txt',
            'fw_acronym_words.txt',
            'fw_adjective_words.txt',
            'fw_colors.txt',
            'fw_compound_bigrams.txt',
            'fw_conjunction_words.txt',
            'fw_determiner_words.txt',
            'fw_dictionary.txt',
            'fw_lifestage_words.txt',
            'fw_material_words.txt',
            'fw_phrases_with_duplicates.txt',
            'fw_preposition_words.txt',
            'fw_ptype_bad_words.txt',
            'fw_ptype_not_whole.txt',
            'fw_ptype_trim_words.txt',
            'fw_quantity_words.txt',
            'fw_size_words.txt',
            'fw_standardized_colors.txt',
            'fw_stop_words.txt',
            'fw_uom_words.txt',
            'espanol.txt',
            'italiano.txt',
            'francais.txt',
            'deutsch.txt',
            'nederlands.txt',
            'norsk.txt',
        ]
        for s_file in a_files:
            assert file_exists(path.join(self.folder, s_file))


class TestSynonyms():
    def test_blank(self):
        assert fwdict.look_up_synonyms('') == set()

    def test_word(self):
        synonyms = {
            'exam', 'run', 'trial_run', 'try', 'prove', 'try_out',
            'psychometric_test', 'tryout', 'mental_testing',
            'examination', 'trial', 'quiz', 'screen', 'mental_test',
            'examine', 'essay', 'test'}
        assert fwdict.look_up_synonyms('test') == synonyms

    def test_phrase(self):
        synonyms = {'test', 'trial', 'tryout', 'trial_run'}
        assert fwdict.look_up_synonyms('trial-run') == synonyms


class TestDefaultDictionary():
    DefaultDict = fwdict.FwDictionary()
    DefaultDict.vocabulary_info()
    DefaultDict.__len__()

    def test_blank(self):
        assert self.DefaultDict.__contains__("") is False
        
    def test_bom_removal(self):
        assert self.DefaultDict.__contains__("gemmosity") is True

    def test_word(self):
        assert self.DefaultDict.__contains__("test") is True

    def test_nonword(self):
        assert self.DefaultDict.__contains__("asdfasd") is False

    def test_vocab_files(self):
        assert len(self.DefaultDict.vocabulary_files) == 25

    def test_get_word(self):
        assert self.DefaultDict.__getitem__('test').__str__() == 'test'

class TestForeignDictionary():
    SpanishDict = fwdict.FwDictionary("spanish")

    def test_blank(self):
        assert self.SpanishDict.__contains__("") is False

    def test_word(self):
        assert self.SpanishDict.__contains__("loco") is True

    def test_accent(self):
        print(self.SpanishDict.dict_name, self.SpanishDict.__len__())
        assert self.SpanishDict.__contains__("mañana") is True

    def test_stripped_accent(self):
        assert self.SpanishDict.__contains__("manana") is True


class TestNGramFreqs():
    datafolder = fwdict._vocab_full_path('fw_dictionary.txt')
    datafolder = folder_from_path(datafolder)
    s_unigrams = path.join(datafolder, "combined-unigrams.txt")
    unigrams = fwdict.read_ngram_freqs(s_unigrams)

    def test_read(self):
        assert self.unigrams
        assert len(self.unigrams) >= 198806

    def test_freqs(self):
        assert self.unigrams[('about',)] == 135489.0
        assert self.unigrams[('ayers',)] == 100.0
        assert self.unigrams[('depositor',)] == 10.0
        
        
class TestCompoundBigrams():
    datafolder = fwdict._vocab_full_path('fw_dictionary.txt')
    datafolder = folder_from_path(datafolder)
    s_bigrams = path.join(datafolder, "fw_compound_bigrams.txt")
    bigrams = fwdict.read_compound_freqs(s_bigrams)

    def test_read(self):
        assert self.bigrams
        assert len(self.bigrams) >= 37439

    def test_freqs(self):
        assert self.bigrams[('in', 'formation')] == (20842.5, 2, 41685)
        assert self.bigrams[('news', 'writer')] == (1.0, 3, 3)
        assert self.bigrams[('stain', 'steel')] == (0.00011277262782777364, 44337, 5)

