﻿import os
import re
from collections import defaultdict, OrderedDict
from nltk.corpus import wordnet as wn
from FwText import strip_unicode_accents, trim

__all__ = ["Word", "FwDictionary", "look_up_synonyms", "read_ngram_freqs",
           "COMBINED_UNIGRAMS", "PHRASES_WITH_DUPLICATES",
           "read_compound_freqs", "COMPOUND_BIGRAMS"]


def _vocab_full_path(path_inside_data):
    '''
    @description: concatenates the path of FwDictionary with /data/
    and the path of a vocabulary file.
    @arg path_inside_data: {str}
    @return: {str}
    '''
    return os.path.join(os.path.dirname(__file__),
                        "data",
                        path_inside_data)


class Word(object):
    """
    @description: Represents a word from the vocabulary.
    Implements word-related methods.
    """
    def __init__(self, text):
        self.text = text.lower()

    def __repr__(self):
        return self.text

    def __str__(self):
        return self.text

    def __eq__(self, other):
        return self.text == other.text

    @property
    def synonyms(self):
        """
        @description: Look up the word's synonyms in Wordnet.
        @return: {set} a set containing the word's synonyms.
        """
        return look_up_synonyms(self.text)


def look_up_synonyms(word):
    """
    @description: Generic function to look up synonyms on wordnet.
    @word: {str} We're looking for synonyms of this word. If it's a
    multiword phrase, look up the phrase using several separators.
    @return: {set} Contains the synonyms of word.
    """
    separators = ["-", "_", " "]
    if any([separator in word for separator in separators]):
        synonyms_list = []
        for separator in separators:
            modified_word = re.sub("(_|-| )", separator, word)
            synonyms_list.extend(_look_up_synonyms(modified_word))
        return set(synonyms_list)

    else:
        return set(_look_up_synonyms(word))


def _look_up_synonyms(word):
    """
    @description helper for look_up_synonyms.
    @arg word: {str} We're looking for synonyms of this word.
    @return: {list} Contains the synonyms of word.
    """
    synlist = wn.synsets(word)
    synset = []
    for synonyms_list in synlist:
        synset.extend(synonyms_list.lemma_names())
    return synset


class FwDictionary(object):
    """
    @description: Holds a set containing FindWatt's vocabulary.
    Implements lookup methods.
    """
    vocabulary_files = {
        # I don't think FwDictionary is able to handle this file
        # "combined_unigrams": {
        #     "path": _vocab_full_path("combined-unigrams.txt"),
        #     "description": ""
        # },
        "fw_abbreviation_words": {
            "path": _vocab_full_path("fw_abbreviation_words.txt"),
            "description": "",
            "strip_unicode": False,
        },
        "fw_accessory_words": {
            "path": _vocab_full_path("fw_accessory_words.txt"),
            "description": "",
            "strip_unicode": False,
        },
        "fw_acronym_words": {
            "path": _vocab_full_path("fw_acronym_words.txt"),
            "description": "",
            "strip_unicode": False,
        },
        "fw_adjective_words": {
            "path": _vocab_full_path("fw_adjective_words.txt"),
            "description": "",
            "strip_unicode": False,
        },
        "fw_colors": {
            "path": _vocab_full_path("fw_colors.txt"),
            "description": "",
            "strip_unicode": False,
        },
        "fw_conjunction_words": {
            "path": _vocab_full_path("fw_conjunction_words.txt"),
            "description": "",
            "strip_unicode": False,
        },
        "fw_determiner_words": {
            "path": _vocab_full_path("fw_determiner_words.txt"),
            "description": "",
            "strip_unicode": False,
        },
        "fw_dictionary": {
            "path": _vocab_full_path("fw_dictionary.txt"),
            "description": "",
            "strip_unicode": False,
        },
        "fw_lifestage_words": {
            "path": _vocab_full_path("fw_lifestage_words.txt"),
            "description": "",
            "strip_unicode": False,
        },
        "fw_material_words": {
            "path": _vocab_full_path("fw_material_words.txt"),
            "description": "",
            "strip_unicode": False,
        },
        "fw_preposition_words": {
            "path": _vocab_full_path("fw_preposition_words.txt"),
            "description": "",
            "strip_unicode": False,
        },
        "fw_ptype_bad_words": {
            "path": _vocab_full_path("fw_ptype_bad_words.txt"),
            "description": "",
            "strip_unicode": False,
        },
        "fw_ptype_not_whole": {
            "path": _vocab_full_path("fw_ptype_not_whole.txt"),
            "description": "",
            "strip_unicode": False,
        },
        "fw_ptype_trim_words": {
            "path": _vocab_full_path("fw_ptype_trim_words.txt"),
            "description": "",
            "strip_unicode": False,
        },
        "fw_quantity_words": {
            "path": _vocab_full_path("fw_quantity_words.txt"),
            "description": "",
            "strip_unicode": False,
        },
        "fw_size_words": {
            "path": _vocab_full_path("fw_size_words.txt"),
            "description": "",
            "strip_unicode": False,
        },
        "fw_standardized_colors": {
            "path": _vocab_full_path("fw_standardized_colors.txt"),
            "description": "",
            "strip_unicode": False,
        },
        "fw_stop_words": {
            "path": _vocab_full_path("fw_stop_words.txt"),
            "description": "",
            "strip_unicode": False,
        },
        "fw_uom_words": {
            "path": _vocab_full_path("fw_uom_words.txt"),
            "description": "",
            "strip_unicode": False,
        },
        "spanish": {
            "path": _vocab_full_path("espanol.txt"),
            "description": "spanish words",
            "strip_unicode": True,
        },
        "italian": {
            "path": _vocab_full_path("italiano.txt"),
            "description": "italian words",
            "strip_unicode": True,
        },
        "french": {
            "path": _vocab_full_path("francais.txt"),
            "description": "french words",
            "strip_unicode": True,
        },
        "german": {
            "path": _vocab_full_path("deutsch.txt"),
            "description": "german words",
            "strip_unicode": True,
        },
        "norwegian": {
            "path": _vocab_full_path("norsk.txt"),
            "description": "norwegian words",
            "strip_unicode": True,
        },
        "dutch": {
            "path": _vocab_full_path("nederlands.txt"),
            "description": "dutch words",
            "strip_unicode": True,
        },
        # "google_shopping_adjectives": {
        #    "path": "google_shopping/adjectives.txt",
        #    "description": ""
        # },
        # "google_shopping_adverbs": {
        #    "path": "google_shopping/adverbs.txt",
        #    "description": ""
        # },
        # "google_shopping_nouns": {
        #    "path": "google_shopping/nouns.txt",
        #    "description": ""
        # },
        # "google_shopping_propns": {
        #    "path": "google_shopping/propns.txt",
        #    "description": ""
        # },
        # "google_shopping_verbs": {
        #    "path": "google_shopping/verbs.txt",
        #    "description": ""
        # },
    }

    def __init__(self, vocabulary_file="fw_dictionary"):
        self.dict_name = vocabulary_file
        if vocabulary_file and vocabulary_file in self.vocabulary_files:
            self.vocab_file = self.vocabulary_files[vocabulary_file]["path"]
        else:
            self.vocab_file = vocabulary_file
            print("The keyword {} doesn't belong to any vocabulary file.\n"
                  "You can use the method vocabulary_info to view available "
                  "vocabulary files.\n"
                  "Trying to use it as a full path...".format(vocabulary_file))
                  
        with open(self.vocab_file, encoding='utf-8') as f:
            self.vocabulary = [word.strip()
                                   for word in f.readlines()
                                   if word.strip()]
            if self.vocabulary[0].startswith('\ufeff'):
                self.vocabulary[0] = self.vocabulary[0][1:]
            
            self.vocabulary = set(self.vocabulary)
            
        if self.vocabulary_files[vocabulary_file].get("strip_unicode"):
            a_unicode_words = [strip_unicode_accents(word) for word in self.vocabulary]
            self.vocabulary.update(a_unicode_words)
        
    @classmethod
    def vocabulary_info(cls, keyword=None):
        '''
        @description: prints information about vocabulary files available.
        @arg keyword: {str} if this is passed, and belongs to valid vocabulary
        file, the function prints information related to the file.
        If not passed the function prints all vocabulary files available.
        '''
        if keyword is None:
            print("Available vocabulary files:")
            for keyword, info in cls.vocabulary_files.items():
                print("{}: {}".format(keyword, info["description"]))
        elif keyword in cls.vocabulary_files.keys():
            print("{}: {}".format(keyword,
                  cls.vocabulary_files[keyword]["description"]))
        print("")

    def __getitem__(self, word):
        """
        @description: Handles 'vocabulary[word]' lookup.
        @arg word: {str} The word to look up.
        @return: {FwDictionary.Word} If the vocabulary contains the
        given word, this returns an object that wraps the word, and
        implements word-related methods.
        Otherwise, it raises KeyError.
        """
        word = word.lower()
        if word in self.vocabulary:
            return Word(word)
        else:
            raise KeyError("{} is not in the vocabulary".format(word))

    def __contains__(self, word):
        """
        @description: Handles 'word in vocabulary' lookup.
        @arg word: {str} The word to look up.
        @return: {bool} Wheter word is in vocabulary.
        """
        return word.lower() in self.vocabulary

    def __len__(self):
        return len(self.vocabulary)


def read_ngram_freqs(s_file, b_tuples=True):
    e_ngrams = defaultdict(int)
    with open(s_file, 'r') as f:
        for line in f:
            a_line = line.split('\t')
            t_ngram = tuple([trim(s_word) for s_word in a_line[1:]])
            if b_tuples:
                e_ngrams[t_ngram] += float(a_line[0])
            else:
                e_ngrams['_'.join(t_ngram)] += float(a_line[0])

    return dict(e_ngrams)


COMBINED_UNIGRAMS = read_ngram_freqs(
    os.path.join(os.path.dirname(__file__),
                 "data/combined-unigrams.txt")
)

PHRASES_WITH_DUPLICATES = read_ngram_freqs(
    os.path.join(os.path.dirname(__file__),
                 "data/fw_phrases_with_duplicates.txt")
)


def read_compound_freqs(s_file):
    e_compounds = OrderedDict()
    with open(s_file, 'r', encoding='utf-8') as f:
        a_lines = list(f)
        a_lines.pop(0)
        for line in a_lines:
            a_line = line.split('\t')
            a_line[-1] = trim(a_line[-1])
            t_ngram = tuple(a_line[-2:])
            t_freq = (float(a_line[0]), int(a_line[1]), int(a_line[2]))
            e_compounds[t_ngram] = t_freq
    return e_compounds


COMPOUND_BIGRAMS = read_compound_freqs(
    os.path.join(os.path.dirname(__file__),
                 "data/fw_compound_bigrams.txt")
)
