﻿304 stainless steel
304/304l
305 stainless steel
316 stainless steel
abs
acetal
acrylic
alpaca
aluminized steel
aluminum
aluminum mesh
aluminum oxide
aluminum oxide/silicon carbide
aluminum/vinyl
ash
bagasse
bamboo
beech wood
beechwood
bi metal
bimetal
bi-metal
bi-metal with cobalt
birch
birch wood
brass
bronze
butyl
calf hair
carbide
carbide steel
carbide tip
carbide tipped
carbide/diamond
carbide-tipped
carbon
carbon fiber
carbon steel
cashmere
cashmere blend
cast
cast aluminum
cast brass
cast bronze
cast copper
cast iron
cast metal
cast-iron
catnip
cedar
ceramic
ceramic alumina
ceramic back
ceramic grain
charmeuse
china
chrome
chrome vanadium
cloth
cobalt
cobalt and tungsten
cobalt steel
cold rolled steel
copper
cork
corrugated
cotton
cotton blend
cotton canvas
cotton poly blend
cotton spandex
cpvc
crocodile
crocodile leather
crocus
cubitron
deer skin
deerskin
delrin
denim
diamond
diamond carbide tip
die cast
die cast zinc
down
drystar
ductile iron
dzr brass
emery
enamel
engineered polymer
epdm
extruded aluminum
fabric
faux fur
faux leather
faux shearling
faux suede
faux-leather
felt
fiber
fiberglass
fire clay
fireclay
fkm
foam
forged brass
forged carbon steel
forged steel
fur
genuine calf hair
genuine leather
glass
goatskin
goose down
gore tex
goretex
gore-tex
granite
hard copper
hard plastic
hardened steel
hardwood
hdpe
heavy duty steel
hi speed steel
high carbon steel
high speed
high speed steel
hook & loop
hook and loop
hot rolled steel
hss
ice hardened
insulated
iron
kevlar
kraft
lambskin
lambswool
latex
latex coated
leather
leatherette
linen
linen blend
linen/cotton
low carbon steel
m7 grade steel
malleable
malleable iron
malleable steel
manila
maple
marble
melamine
memory foam
merino
merino wool
mesh
mesh leather
mesh lining
mesh/textile
metal
milk silk
milk-silk
mixed media
mohair
molybdenum steel
multi material
multi-material
mylar
nbr
neodymium
neoprene
neo-prene
nickel chrome plated
nickel chromium steel
nitrile
non metallic
non-metallic
nontoxic ink
nylon
nylon/china
nylon/polyester
oak
olefin
ox hair
ox-hair
padded
paper
paper/poly
pex
phenolic
pigskin
pima cotton
pine
plant fibers
plastic
plastic/cloth
poly
poly alloy
poly cotton
poly paper
poly synthetic
poly/bristle
poly/cotton
polyamide
polybethylene
polybutylene
polycarbonate
polycotton
polyester
polyethylene
polymer
polypropylene
polystyrene
poly-synthetic
polyurethane
polyvinyl
poplar
porcelain
porcelain top
pork skin
ptfe
pvc
pvc coated
pvc/rubber
rayon
recycled plastic
red brass
redwood
resin
rock carbide
rubber
rubber latex
rubber/vinyl
sable
sbr
shearling
sheepskin
silicon bronze
silicon carbide
silicone
silk
silks
sisal
snakeskin
solid brass
solid bronze
solid carbide
spandex
split leather
ss
stainless
stainless steel
stainless-steel
stamped aluminum
stamped metal
stamped steel
steel
stencil board
styrene
suede
sv
synthetic
synthetic leather
tampico
teak
teflon
textured stone
thermoplastic
thermoset
tin
titanium
titanium carbide
tpe
tungsten
tungsten carbide
tyvek
urea
urethane
vanadium steel
vegan leather
vinyl
vinyl coated
vinyl latex
viscount
vitreous china
walnut
wicker
wire
wood
wooden
wool
wool blend
wool/cashmere
wool/polyester
wrot
wrot copper
wrot steel
wrought iron
wrought steel
yellow brass
zinc
zinc alloy
zinc die cast
zinc diecast
zirconia
zirconia alumina
zirconium oxide
